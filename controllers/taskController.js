// contains functions and business logic of our express js application
// all operations it can do will be placed in this file.

// allows us to access/use the contents of "task.js" file in /models folder.
					const Task = require("../models/task.js");

// defines the functions to be used in the "taskRoutes.js"
					module.exports.getAllTasks = () => {
						return Task.find ( { } ).then(result => {
							return result;
						})
					}

// function for creating a task
					module.exports.createTask = (requestBody) => {
						let newTask = new Task ({
							name: requestBody.name
						});
						return newTask.save().then((task, error) => {
							if (error) {
								console.log (error)
								return false;
							} else {
								return task;
							}
						})
					}

// findByIdAndRemove - is a mongoose method that searches for the documents using the _id properties; after finding a document, the job of this method is to remove the document
					module.exports.deleteTask = (taskId) => {
						return Task.findByIdAndRemove(taskId).then((result, error) => {
							if(error){
								console.log(error);
								return false;
							} else {
								return result
							}
						})
					}

					module.exports.updateTask = (taskId, newContent) => {
						return Task.findById(taskId).then((result, error) => {
							if (error){
								return false;
							} else {
								result.name = newContent.name
								return result.save().then((updatedTask, error) => {
									if (error) {
										console.log (error)
										return false
									} else {
										return updatedTask
									}
								})
							}
						})
					}

// ----------------------------------------------ACTIVITY-------------------------------------------------------------------------------

					module.exports.getOneTask = (taskId) => {
						return Task.findById(taskId).then(result => {
							return result;
						})
					}

					module.exports.completeTask = (taskId) =>{
						return Task.findById (taskId).then((result, error) => {
							if (error) {
								console.log (error)
								return false
							}
							result.status = "complete"
							return result.save().then((newStatus, error) => {
								if (error) {
									console.log (error);
									return false;
								} else {
									return newStatus;
								}
							})
						})
					}

					
