// basic imports
					const express = require("express");
					const mongoose = require("mongoose");
// allows us to use contents of taskRoutes.js in the routes folder
					const taskRoutes = require("./routes/taskRoutes.js");

// server setup
					const app = express();
					const port = 3000;

// MongoDB connection
					mongoose.connect("mongodb+srv://giolimcaoco:Yx6WOVh2BqmAfb7y@wdc028-course-booking.bczl9fp.mongodb.net/b190-to-do?retryWrites=true&w=majority",
						{
							useNewUrlParser: true, 
							useUnifiedTopology: true 
						}
					);

					let db = mongoose.connection;					
					db.on("error", console.error.bind(console, "Connection Error"));
					db.once("open", () => console.log("We're connected to the database"));

					app.use(express.json());
					app.use(express.urlencoded({extended:true}));

					app.use("/tasks", taskRoutes) // allows all the task routes created in the "taskRoutes.js" file to use "/tasks" route

// Server Running Confirmation
					app.listen(port, () => console.log(`Server running at port: ${port}`));