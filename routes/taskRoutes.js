// contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server
// we have to use Router method inside express

					const express = require("express");
					
// this allows us to create/access HTTP method middlewares that makes it easier to create routing system
					const router = express.Router();

// this allows us to use the contents of "taskController.js" in "controllers" folder
					const taskController = require("../controllers/taskController.js")

// route to get all tasks
/*
	get request to be sent at localhost:3000/tasks
*/
// since the controller did not send any response, the task falls to the taskRoutes.js to send a response that is received from the controllers and send it to the frontend.
					router.get("/", (req, res) => {
						taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
					})

					router.post("/", (req, res) => {
						taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
					})

					router.delete("/:id", (req, res) => {
						taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
					})

//  req.params.id - retrieves the taskId of the document to be updated
//  req.body determines what updates are to be done in the document
					router.put("/:id", (req, res) => {
						taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
					})

// module.exports - allows us to export the file where it is inserted to be used by other files such as app.js/index.js
					module.exports = router; 

// ------------------------------------------------------------------ACTIVITY--------------------------------------------------------------------

/*1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S36.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.*/

					router.get("/:id", (req, res) => {
						taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
					})

					router.put("/:id/complete", (req, res) => {
						taskController.completeTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
					})

